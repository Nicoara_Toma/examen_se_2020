import javax.swing.*;

public class Test extends JFrame {
    JTextField textField;
    JButton button;
    JButton reset;
    static int counter=1;
    int resetnr = 0;
    Test (){
        setTitle("Counter button");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300,300);
        setVisible(true);
    }
    public void init(){
        this.setLayout(null);
        textField=new JTextField("");
        textField.setBounds(110,30,60,40);

        button=new JButton("Press to count");
        button.setBounds(90,100,90,40);
        button.addActionListener(e->{
                    textField.setText(String.valueOf(counter));
                    counter++;
                }
        );
        reset = new JButton("Press to reset");
        reset.setBounds(90,200,80,40);
        reset.addActionListener(e->{
            textField.setText(String.valueOf(resetnr));
            counter = 1;
        });

        add(textField);
        add(button);
        add(reset);
    }

    public static void main(String[] args) {
        Test counterWithButton=new Test();
    }
}