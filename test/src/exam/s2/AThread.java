package exam.s2;

public abstract class AThread {
    // class attributes
    protected String name;
    protected int messToDisplay;
    protected int secBetweenMess;

    // constr
    public AThread(String name, int messToDisplay, int secBetweenMess) {
        this.name = name;
        this.messToDisplay = messToDisplay;
        this.secBetweenMess = secBetweenMess;
    }

    // abstract method to be implemented by child classes
    public abstract void printMess(int indexMess);
}
