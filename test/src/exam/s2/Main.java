package exam.s2;

public class Main {

    public static void main(String[] args) {
        // create the 2 threads
        AThread1 aThread1 = new AThread1("AThread1", 8, 3);
        AThread1 aThread2 = new AThread1("AThread2", 8, 3);
        Thread aThread11 = new Thread(aThread1);
        Thread aThread22 = new Thread(aThread2);

        // start exec the threads
        aThread11.start();
        aThread22.start();
    }

}