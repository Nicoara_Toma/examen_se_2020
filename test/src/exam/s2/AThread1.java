package exam.s2;

public class AThread1 extends AThread implements Runnable {

    // constr
    public AThread1(String name, int messToDisplay, int secBetweenMess) {
        // call super constr
        super(name, messToDisplay, secBetweenMess);
    }

    public void run() {
        // end exec mess
        System.out.println("Thread " + name + " started execution!");

        for (int i = 0; i < this.messToDisplay; i++) {
            // print mess
            printMess(i);

            // wait required no of s
            try {
                Thread.sleep(secBetweenMess * 1000);	// ms to sec
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        // end exec mess
        System.out.println("Thread " + name + " finished execution!");
    }

    // printing method
    @Override
    public void printMess(int indexMess) {
        System.out.println("Thread " + this.name + " printing mess nr: " + indexMess);
    }

}