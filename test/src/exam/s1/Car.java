package exam.s1;

public class Car extends Vehicle {
    private String color;
    private Windshield windshield;
    private Wheel wheel1;
    private Wheel wheel2;
    private Wheel wheel3;
    private Wheel wheel4;

    CarMaker carmaker;

    public Car(CarMaker carmaker) {
        this.carmaker = carmaker;
    }


    public Car(Windshield windshield, Wheel wheel1, Wheel wheel2, Wheel wheel3, Wheel wheel4){
        this.windshield = windshield;
        this.wheel1 = wheel1;
        this.wheel2 = wheel2;
        this.wheel3 = wheel3;
        this.wheel4 = wheel4;
    }

    public void start(){
        System.out.println("Start");
    }
    public void stop(){
        System.out.println("Stop");
    }
    public void go(){
        System.out.println("Go");
    }
}
